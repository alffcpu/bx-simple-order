<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_ORDER_NAME"),
	"DESCRIPTION" => GetMessage("T_ORDER_DESCRIPTION"),
	"ICON" => "/images/icon.png",
	"SORT" => 100,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content"
	),
);

?>