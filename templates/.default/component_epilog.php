<?php
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(str_ireplace('component_epilog', 'template', __FILE__));

$APPLICATION->SetAdditionalCSS('/bitrix/css/main/bootstrap.min.css', true);
$APPLICATION->SetAdditionalCSS('/bitrix/css/main/font-awesome.min.css', true);

$APPLICATION->AddHeadScript($templateFolder . '/js/select2/js/select2.full.min.js');
$APPLICATION->AddHeadScript($templateFolder . '/js/select2/js/i18n/ru.js');
$APPLICATION->SetAdditionalCSS($templateFolder . '/js/select2/css/select2.min.css', true);
$APPLICATION->SetAdditionalCSS($templateFolder . '/js/select2/css/select2-bootstrap.min.css', true);

$APPLICATION->AddHeadScript($templateFolder . '/js/touchspin/jquery.bootstrap-touchspin.min.js');
$APPLICATION->SetAdditionalCSS($templateFolder . '/js/touchspin/jquery.bootstrap-touchspin.min.css', true);

$APPLICATION->AddHeadScript($templateFolder . '/js/validate/jquery.validate.min.js');
$APPLICATION->AddHeadScript($templateFolder . '/js/validate/additional-methods.min.js');
$APPLICATION->AddHeadScript($templateFolder . '/js/validate/localization/messages_ru.min.js');

$APPLICATION->AddHeadScript($templateFolder . '/js/jquery.mask.min.js');

$js_message = function($code) {
	return str_replace(["\n", "\r", "\t", "'"], '', Loc::getMessage($code));
}
?>
<script type="text/javascript">
(function(w) {
	var formId = '<?=isset($templateData['FORM_ID']) ? $templateData['FORM_ID']: ''?>';
	var ocFormStart = function() {
		if (!formId || !w.OneClickOrderForm || !w.jQuery) return false;

		var form = new OneClickOrderForm({
			formId: '<?=$templateData['FORM_ID']?>',
			path: '<?=$APPLICATION->GetCurPage()?>',
			componentPath: '<?=$componentPath?>',
			iblockId: <?=intval($arParams['IBLOCK_ID'])?>,
			eventCode: '<?=$arParams['EVENT_CODE']?>',
			useRatio: <?=$arParams['USE_RATIO'] == 'Y' ? 'true' : 'false'?>,
			itemImages: <?=$arParams['ITEM_IMAGES'] == 'Y' ? 'true' : 'false'?>,
			sessId: '<?=bitrix_sessid()?>',
			captcha: '<?=$APPLICATION->CaptchaGetCode()?>',
			messages: {
				LOAD_LIST_ERROR: '<?=$js_message('OC_FORM_LOAD_LIST_ERROR')?>',
				TOTAL_TEMPLATE: '<?=$js_message('OC_FORM_SUBTOTAL')?>|<?=$js_message('OC_FORM_SUBTOTAL_OLD')?>',
				PHONE_MASK: '<?=$js_message('OC_FORM_PHONE_MASK')?>',
				SUCCESS: '<?=$js_message('OC_FORM_SUCCESS')?>'
			}
		});
		form.start();
		return true
	};
	if (!ocFormStart()) {
		document.addEventListener("DOMContentLoaded", function() {
			if (!ocFormStart()) {
				try {
					document.getElementById(formId).innerHTML = '<div class="alert alert-danger" style="margin:0"><?=Loc::getMessage('OC_FORM_INIT_FAILS')?></div>';
				} catch (e) {}
				console.log('OneClickOrderForm: ', typeof w.OneClickOrderForm);
				console.log('jQuery: ', typeof w.jQuery);
				console.log('formId: ', formId);
			}
		});
	}
})(window);
</script>