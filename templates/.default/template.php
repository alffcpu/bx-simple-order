<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
$form_id = 'oc-form-'.md5(mt_rand(1, 9999).time());
$templateData['FORM_ID'] = $form_id;
?>
<div class="oc-form-container">
	<form class="oc-form-form" id="<?=$form_id?>">
		<div class="oc-form-messages"></div>
		<h4><?=Loc::getMessage('OC_FORM_PRODUCT_TITLE')?></h4>
		<div class="form-group row oc-form-row-relative">
			<div class="col-xs-12">
				<select name="item_id" disabled class="form-control oc-form-item_id-input" placeholder="<?=Loc::getMessage('OC_FORM_PLEASE_WAIT')?>" data-placeholder="<?=Loc::getMessage('OC_FORM_SELECT_PLACEHOLDER')?>"><option><?=Loc::getMessage('OC_FORM_PLEASE_WAIT')?></option></select>
			</div>
			<div class="oc-form-overlay"><div class="oc-form-preloader"><i class="fa fa-spinner fa-pulse fa-2x"></i></div></div>
		</div>
		<div class="form-group row">
			<div class="col-md-3 col-xs-12">
				<input type="text" disabled name="qty" class="form-control oc-form-quantity-input" placeholder="<?=Loc::getMessage('OC_FORM_QUANTITY')?>">
			</div>
			<div class="col-md-3 col-xs-12">
				<div class="oc-form-measure-info"></div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="oc-form-price-totals">

				</div>
			</div>
		</div>
		<h4><?=Loc::getMessage('OC_FORM_BUYER_TITLE')?></h4>
		<div class="form-group row">
			<div class="col-xs-12">
				<input type="text" required name="name" class="form-control oc-form-buyer-name" placeholder="<?=Loc::getMessage('OC_FORM_BUYER_NAME')?>">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6 col-xs-12">
				<input type="email" required name="email" class="form-control oc-form-buyer-email" placeholder="<?=Loc::getMessage('OC_FORM_BUYER_EMAIL')?>">
			</div>
			<div class="col-md-6 col-xs-12">
				<input type="text" name="phone" class="form-control oc-form-buyer-phone" placeholder="<?=Loc::getMessage('OC_FORM_BUYER_PHONE')?>">
			</div>
		</div>
		<? if ($arParams['USE_CAPTCHA'] == 'Y'): ?>
		<div class="form-group row oc-form-captach-row">
			<div class="col-xs-6 col-md-3">
				<div class="oc-form-captach-cell">
					<img src="" data-src="/bitrix/tools/captcha.php?captcha_sid=" alt="">
					<span class="oc-form-reload-captcha-trigger" type="button"><i class="fa fa-refresh"></i> <?=Loc::getMessage('OC_FORM_RELOAD_CAPTCHA')?></span>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<input type="text" required maxlength="10" name="captcha" class="form-control oc-form-captcha-input" placeholder="<?=Loc::getMessage('OC_FORM_CAPTCHA_PLACEHOLDER')?>">
			</div>
		</div>
		<? endif ?>
		<div class="oc-form-row-controls oc-form-row-hidden">
			<button type="submit" class="btn btn-primary"><?=Loc::getMessage('OC_FORM_SUBMIT')?></button>
		</div>
		<div class="well well-sm oc-form-note"><?=Loc::getMessage('OC_FORM_REQUIRED_NOTE')?></div>
	</form>
	<div class="oc-form-overlay"><div class="oc-form-preloader"><i class="fa fa-spinner fa-pulse fa-3x"></i></div></div>
</div>