(function(w, d) {
	if (!w.jQuery) {
		var script = d.createElement('script');
		script.type = "text/javascript";
		script.src = "https://code.jquery.com/jquery-1.12.4.min.js";
		d.getElementsByTagName('head')[0].appendChild(script);
	}
})(window, document);

(function(window) {

	if (window.OneClickOrderForm) return;

	var OneClickOrderForm = function(params) {
		this.params = params;
		this.$form = $('#' + params.formId);
		this.$select = this.$form.find('[name="item_id"]');
		this.$qty = this.$form.find('[name="qty"]');
		this.$measure = this.$form.find('.oc-form-measure-info');
		this.$captachTrigger = this.$form.find('.oc-form-reload-captcha-trigger');
		this.$price = this.$form.find('.oc-form-price-totals');
		this.$controls = this.$form.find('.oc-form-row-controls');
		this.$info = this.$form.find('.oc-form-messages');
		this.initRetry = 5;
		this.items = [];
		this.captcha = this.params.captcha;
		this.priceTimeoutId = 0;
		this.userInfo = null;
	};

	OneClickOrderForm.prototype.getMessage = function (code) {
		return this.params.messages[code] || "{" + code.toUpperCase() + "}";
	};

	OneClickOrderForm.prototype.fatalError = function (html) {
		this.$form.html('<div class="alert alert-danger" style="margin:0">' + html + '</div>');
	};

	OneClickOrderForm.prototype.initQty = function (opts) {
		opts = opts || {};
		var defaults = {
			verticalbuttons: true,
			verticalupclass: 'fa fa-angle-up',
			verticaldownclass: 'fa fa-angle-down',
			buttondown_class: 'btn btn-default',
			buttonup_class: 'btn btn-default'
		};
		this.$qty.TouchSpin($.extend(defaults, opts));
		this.$form.find('.bootstrap-touchspin-up, .bootstrap-touchspin-down').html('');
		this.$qty.on('change', this.qtyChangeHandler.bind(this));
		this.$qty.on('blur', this.qtyBlurHandler.bind(this));
	};

	OneClickOrderForm.prototype.updateQty = function (opts) {
		this.$qty.trigger('touchspin.updatesettings', opts);
	};

	OneClickOrderForm.prototype.setQty = function (value) {
		this.$qty.val(value).trigger('change');
	};

	OneClickOrderForm.prototype.loadProductsList = function () {
		var self = this;
		$.post(
			this.params.path,
			{
				action: 'items_list',
				dataType: 'json'
			}
		).done(function(response) {
			if (response.items) {
				self.items = response.items;
				self.createProductsList();

				self.userInfo = response.user;

				var $captcha = self.$form.find('.oc-form-captach-row');
				if (self.userInfo) {
					$captcha.hide();
					self.autoFillUser();
				} else {
					$captcha.show();
				}
			}
		}).error(function(e) {
			if (self.initRetry--) {
				setTimeout(function () {
					self.loadProductsList();
				}, 1000);
			} else {
				self.fatalError(self.getMessage('LOAD_LIST_ERROR'));
			}
		});
	};

	OneClickOrderForm.prototype.autoFillUser = function () {
		var self = this;
		$.each(self.userInfo, function(index, value) {
			var $input = self.$form.find('input[name="' + index + '"]');
			if (!$.trim($input.val())) {
				$input.val(value).trigger('input');
			}
		});
	};

	OneClickOrderForm.prototype.createProductsList = function () {
		var self = this;
		var html = '<option></option>';
		var formatOption = function (state) {
			if (!state.id) {
				return state.text;
			}
			var item = self.items[state.element.value];
			var formatedPrice = self.displayPrice(item.CATALOG_PURCHASING_PRICE, item.CATALOG_PURCHASING_CURRENCY);
			return $(
				'<span>' +
				(self.params.itemImages ? '<img src="' + item['PICTURE'] + '" class="oc-form-option-image" alt="" /> ' : '') +
				state.text +
				( formatedPrice ? " &mdash; " + formatedPrice : '') +
				'</span>'
			);
		};
		$.each(this.items, function(index, item) {
			html += '<option value="' + item.ID + '">' + item.NAME + '</option>';
		});
		this.$select.html(html);
		this.$select.closest('.oc-form-row-relative').find('.oc-form-overlay').remove();
		this.$select.select2({
			templateResult: formatOption,
			placeholder: this.$select.data('placeholder')
		});
		this.$select.prop('disabled', false).on('change', function() {
			self.selectChangeHandler(this, self)
		});
	};

	OneClickOrderForm.prototype.displayPrice = function (price, currency) {
		if (!price) return '';
		currency = currency || 'RUB';
		var symbol = ' <i class="fa fa-' + currency.toLocaleLowerCase() + '"></i>';
		return price + symbol;
	};

	OneClickOrderForm.prototype.getQtyMinValue = function (item) {
		var minValue = 1;
		if (this.params.useRatio) {
			minValue = item.MEASURE_RATIO;
		}
		return minValue;
	};

	OneClickOrderForm.prototype.selectChangeHandler = function (element, self) {
		if (element.value) {
			var item = self.items[element.value];
			self.$controls.removeClass('oc-form-row-hidden');
			self.$qty.prop('disabled', false);
			var minValue = self.getQtyMinValue(item);
			var measureInfo = item.MEASURE_TITLE || (item.MEASURE_SYMBOL || "");
			self.updateQty({
				min: minValue,
				initval: minValue,
				max: item.CATALOG_QUANTITY || 100000,
				step: minValue
			});
			self.setQty(minValue);
			this.$measure.html('<span class="oc-form-measure-symbol"><i class="fa fa-long-arrow-right"></i> ' + measureInfo.toLowerCase() + '</span>')
		}
	};

	OneClickOrderForm.prototype.validateForm = function () {
		var self = this;
		self.$form.validate({
			submitHandler: self.sendFormData.bind(self)
		});
	};

	OneClickOrderForm.prototype.sendFormData = function (form) {
		var self = this;
		var $overlayElement = self.$form.parent();
		self.localOverlay($overlayElement);
		var formData = self.$form.serializeArray();
		$.each({
			use_ratio: self.params.useRatio ? 'Y' : 'N',
			action: 'submit_order',
			sess_id: self.params.sessId,
			captcha_sid: self.captcha
		}, function(index, value) {
			formData.push({
					name: index,
					value: value
				}
			);
		});

		self.$info.html('');
		$.post(
			self.params.path,
			formData
		).always(function () {
			self.removeLocalOverlay($overlayElement);
		}).done(function (response) {
			if (response.error) {
				self.$info.html('<div class="alert alert-danger"><button onclick="$(this).parent().remove();" type="button" class="close"><i class="fa fa-times"></i></button><p>' + response.error.join('</p><p>') + '</p></div>');
			} else {
				var message = self.getMessage('SUCCESS');
				$.each(response.fields, function(field, value) {
					message = self.replaceAll(message, '#'+field+'#', value);
				});
				self.$form.html('<div class="alert alert-success oc-form-success-message">' + message + '</div>');
			}
		});
	};

	OneClickOrderForm.prototype.replaceAll = function(str, find, replace) {
		return str.replace(new RegExp(find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), 'g'), replace);
	};

	OneClickOrderForm.prototype.qtyChangeHandler = function() {
		var self = this;
		var $container = self.$price;
		var item_id = self.$select.val();
		var item = self.items[item_id];

		clearTimeout(self.priceTimeoutId);
		self.priceTimeoutId = setTimeout(function () {
			$container.html('<i class="fa fa-spinner fa-pulse"></i>');
			$.post(
				self.params.componentPath + '/price.php',
				{
					action: 'optimal_price',
					product_id: item_id,
					product_qty: self.$qty.val(),
					template: self.getMessage('TOTAL_TEMPLATE'),
					currency: item.CATALOG_PURCHASING_CURRENCY || 'RUB',
					use_ratio: self.params.useRatio ? 'Y' : 'N'
				}
			).always(function () {
				$container.html('');
			}).done(function (response) {
				if (response.text) {
					var text = response.text.split('|');
					$container.html('<div class="alert alert-warning alert-sm">' + text[0] + ' <span class="oc-form-subtotal-discount">' + (text[1] || '') + '</span></div>');
				}
			});
		}, 500);
	};

	OneClickOrderForm.prototype.qtyBlurHandler = function() {
		var value = $.trim(this.$qty.val());
		if (!value) {
			var itemId = this.$select.val();
			var item = this.items[itemId];
			this.setQty(this.getQtyMinValue(item));
		}
	};

	OneClickOrderForm.prototype.setCaptchaReload = function () {
		var self = this;
		self.$captachTrigger.on('click', function(e) {
			var $overlayElement = self.$captachTrigger.closest('.oc-form-captach-cell');
			var $image = $overlayElement.find('img');
			self.localOverlay($overlayElement);
			$image.css({visibility: 'hidden'});
			$.post(
				self.params.componentPath + '/captcha.php',
				{
					action: 'reload_captcha',
					sess_id: self.params.sessId
				}
			).always(function() {
				$image.css({visibility: 'visible'});
				self.removeLocalOverlay($overlayElement);
			}).done(function(response) {
				self.setCaptchaImage(response.captcha);
				self.captcha = response.captcha;
			});
		});
	};

	OneClickOrderForm.prototype.setCaptchaImage = function (caprtchId) {
		var $image = this.$captachTrigger.closest('.oc-form-captach-cell').find('img');
		var src = $image.data('src');
		$image.attr('src', src + caprtchId);
	};

	OneClickOrderForm.prototype.localOverlay = function ($element) {
		$element.append('<div class="oc-form-overlay" style="display:block"><div class="oc-form-preloader"><i class="fa fa-spinner fa-pulse fa-2x"></i></div></div>');
	};

	OneClickOrderForm.prototype.removeLocalOverlay = function ($element) {
		$element.find('.oc-form-overlay').remove();
	};

	OneClickOrderForm.prototype.setPhoneMask = function () {
		this.$form.find('[name="phone"]').mask(this.getMessage('PHONE_MASK'));
	};

	OneClickOrderForm.prototype.setCaptchaInputTransform = function () {
		this.$form.find('[name="captcha"]').on('input', function(e) {
			this.value = this.value.toUpperCase();
		});
	};

	OneClickOrderForm.prototype.start = function () {
		this.initQty();
		this.setPhoneMask();
		this.loadProductsList();
		this.validateForm();
		this.setCaptchaImage(this.captcha);
		this.setCaptchaReload();
		this.setCaptchaInputTransform();
	};

	window.OneClickOrderForm = OneClickOrderForm;

})(window);
