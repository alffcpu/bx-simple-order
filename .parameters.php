<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_IBLOCK_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"EVENT_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_EVENT_CODE"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"USE_RATIO" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_USE_RATIO"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"USE_CAPTCHA" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_USE_CAPTCHA"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		"ITEM_IMAGES" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_IMAGES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"ORDER_CODE_NAME" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_CODE_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"ORDER_CODE_EMAIL" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_CODE_EMAIL"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"ORDER_CODE_PHONE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_CODE_PHONE"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"ORDER_PERSON_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_PERSON_TYPE"),
			"TYPE" => "STRING",
			"DEFAULT" => '1',
		),
		"ORDER_DEFAULT_USER" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_DEFAULT_USER"),
			"TYPE" => "STRING",
			"DEFAULT" => '1',
		),
		"ORDER_PAYMENT_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_PAYMENT_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '1',
		),
		"ORDER_DELIVERY_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("OCF_ORDER_PAYMENT_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"CACHE_TIME"  =>  array("DEFAULT" => 36000),
	)
);
