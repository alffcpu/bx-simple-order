<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
	Bitrix\Iblock,
	Bitrix\Main\Context,
	Bitrix\Catalog\ProductTable,
	Bitrix\Sale\Basket,
	Bitrix\Sale\Order,
	Bitrix\Sale\Delivery,
	Bitrix\Sale\PaySystem,
	Bitrix\Main\Localization\Loc;

class COneClickOrderForm extends CBitrixComponent {

	/** @var $arResult */
	/** @var $arParams */
	private $action = null;
	private $is_ajax = null;
	private $form_data = [];
	private $output = '';
	private $item = [];
	private $email_fields = [];

	/* order defaults */
	private $person_type = 1;
	private $default_user = 1;
	private $payment_id = 1;

	/**
	 * @return string
	 */
	private function methodName() {
		$name = [];
		foreach (explode('_', $this->action) as $key => $value) {
			$name[] = $key ? ucfirst($value) : $value;
		}
		return implode('', $name);
	}

	/**
	 * @return mixed|void
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function executeComponent() {

		global $APPLICATION;

		if (!intval($this->arParams['IBLOCK_ID'])) {
			ShowError(Loc::getMessage('OC_FORM_IBLOCK_ID_ERROR'));
			return;
		}

		$required_modules = [
			'iblock' => 'OC_FORM_IBLOCK_MODULE_NOT_INSTALLED',
			'catalog' => 'OC_FORM_CATALOG_MODULE_NOT_INSTALLED',
			'sale' => 'OC_FORM_SALE_MODULE_NOT_INSTALLED'
		];
		foreach ($required_modules as $module => $error_message) {
			if(!Loader::includeModule($module)) {
				ShowError(Loc::getMessage($error_message));
				return;
			}
		}

		$context = Context::getCurrent();
		$request = $context->getRequest();
		$this->is_ajax = $request->isAjaxRequest();
		$this->action = isset($_REQUEST['action']) ? htmlspecialcharsex($_REQUEST['action']) : 'display_from';

		$this->arResult = [
			'is_ajax' => $this->is_ajax
		];

		if($this->startResultCache(false, ($this->is_ajax ? 'POST' : 'GET').'_'.$this->action))  {

			if ($this->is_ajax && $this->action) {
				$method = $this->methodName();
				if (method_exists($this, $method)) {
					$prevent = $this->{$method}();
					if ($prevent !== FALSE) {
						$this->response();
					}
				} else {
					$this->AbortResultCache();
					$this->arResult['error'] = Loc::getMessage('OC_FORM_METHOD_NAME_ERROR');
					$this->arResult['wrong_action'] = $method;
					$this->response();
				}
			} else {
				$this->includeComponentTemplate();
				return;
			}
		}

	}

	/**
	 * action: 'get_list'
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 */
	private function itemsList() {
		$arSelect = [
			'ID',
			'IBLOCK_ID',
			'ACTIVE_FROM',
			'DETAIL_PAGE_URL',
			'NAME',
			'PREVIEW_PICTURE',
			'DETAIL_PICTURE',
		];
		$arFilter = array (
			'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'CATALOG_AVAILABLE' => 'Y',
			'SECTION_ACTIVE' => 'Y',
			'SECTION_GLOBAL_ACTIVE' => 'Y'
		);

		if (isset($this->arParams['FILTER_ID'])) {
			$filter_id = $this->arParams['FILTER_ID'];
			if (!is_array($filter_id) && $filter_id_as_string = trim($filter_id)) {
				$filter_id = [];
				foreach (explode(',', $filter_id_as_string) as $value) {
					if ($value = intval(trim($value))) $filter_id[] = $value;
				}
			}
			$arFilter['ID'] = $filter_id;
		}

		$arOrder = ['NAME' => 'ASC', 'SORT' => 'ASC'];

		$this->arResult['items'] = [];

		$item_ids = [];
		$dbr_items = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
		while($arItem = $dbr_items->GetNext()) {

			$item = [];

			$item['PICTURE'] = null;
			if ($arItem['PREVIEW_PICTURE'] || $arItem['DETAIL_PICTURE']) {
				$item['PICTURE'] = CFile::GetPath( $arItem['PREVIEW_PICTURE'] ? $arItem['PREVIEW_PICTURE'] : $arItem['DETAIL_PICTURE'] );
			}

			foreach ([
						 'ID',
						 'ACTIVE_FROM',
						 'CATALOG_MEASURE',
						 'CATALOG_PURCHASING_CURRENCY',
						 'CATALOG_PURCHASING_PRICE',
						 'CATALOG_QUANTITY',
						 'CATALOG_QUANTITY_TRACE',
						 'DETAIL_PAGE_URL',
						 'NAME',
						 'MEASURE'
					 ] as $field) {
				if (isset($arItem[$field])) $item[$field] = $arItem[$field];
			}

			$item_ids[] = $item['ID'];
			$this->arResult["items"][$item['ID']] = $item;
		}

		$measures = ProductTable::getCurrentRatioWithMeasure($item_ids);
		foreach ($measures as $item_id => $measure) {
			if (isset($this->arResult['items'][$item_id])) {
				$this->arResult['items'][$item_id]['MEASURE_RATIO'] = $measure['RATIO'];
				$this->arResult['items'][$item_id]['MEASURE_SYMBOL'] = LANGUAGE_ID == 'ru' ? $measure['MEASURE']['SYMBOL_RUS'] : $measure['MEASURE']['SYMBOL_INTL'];
				$this->arResult['items'][$item_id]['MEASURE_TITLE'] = $measure['MEASURE']['MEASURE_TITLE'];
			};
		}

		$this->SetResultCacheKeys(["items"]);
		$this->getUserInfo();
	}

	private function getUserInfo() {
		global $USER;
		if ($USER->IsAuthorized()) {
			$dbr_user = CUser::GetByID($USER->GetID());
			$user = $dbr_user->Fetch();
			$this->arResult['user'] = [
				'name' => $USER->GetFullName(),
				'email' => $USER->GetEmail(),
				'phone' => $user['PERSONAL_PHONE'] ? $user['PERSONAL_PHONE'] : $user['WORK_PHONE']
			];
		} else {
			$this->arResult['user'] = null;
		}
	}

	/**
	 * action: submit_order
	 *
	 */
	private function submitOrder() {
		$this->abortResultCache();
		$validation_errors = $this->validateFormData();
		if (!empty($validation_errors)) {
			$this->arResult['error'] = $validation_errors;
			return;
		}
		try {
			$order_id = $this->createOrder();
			$this->sendEmail($order_id);
			$this->arResult = [
				'success' => true,
				'order_id' => $order_id,
				'fields' => $this->email_fields
			];
		} catch (Exception $exception) {
			$this->arResult['error'] = [$exception->getMessage()];
		}
	}

	/*
#QUANTITY_RATIO# - ���������� � ������ ratio
#PRICE# - ����
#PRICE_FORMATED# - ���� � �������
#NAME# - �������� ������
#DETAIL_PAGE_URL# - �������� ������
#FORM_ITEM_ID# - ID ������
#FORM_QTY# - ���������� ������
#FORM_NAME# - ���
#FORM_EMAIL# - Email
#FORM_PHONE# - �������
#ORDER_ID# - ����� ������
#MEASURE_TITLE# - ������� ���������
#MEASURE_SYMBOL# - ������� ��������� ����������
	*/

	/**
	 * @param $order_id
	 * @throws \Bitrix\Main\ArgumentException
	 */
	private function sendEmail($order_id) {
		$site_id = Context::getCurrent()->getSite();
		$fields = ['ORDER_ID' => $order_id];
		foreach (['QUANTITY_RATIO', 'PRICE', 'PRICE_FORMATED', 'NAME', 'DETAIL_PAGE_URL'] as $field) {
			$fields[$field] = isset($this->item[$field]) ? $this->item[$field] : '';
		}
		foreach ($this->form_data as $field => $value) {
			$fields['FORM_'.strtoupper($field)] = $value;
		}
		$item_id = $this->item['ID'];
		$fields['MEASURE_TITLE'] = $fields['MEASURE_SYMBOL'] = '';
		$measure = ProductTable::getCurrentRatioWithMeasure([$item_id]);
		if (!empty($measure)) {
			$measure = $measure[$item_id];
			$fields['MEASURE_TITLE'] = $measure['MEASURE']['MEASURE_TITLE'];
			$fields['MEASURE_SYMBOL'] = LANGUAGE_ID == 'ru' ? $measure['MEASURE']['SYMBOL_RUS'] : $measure['MEASURE']['SYMBOL_INTL'];
		}
		$this->email_fields = $fields;
		if ($event_code = trim($this->arParams['EVENT_CODE'])) {
			CEvent::Send($event_code, $site_id, $fields, "N");
		}
	}

	/**
	 * @throws Exception
	 */
	private function createOrder() {

		global $USER;

		$item_id = $this->form_data['item_id'];
		$site_id = Context::getCurrent()->getSite();

		$dbr_item = CIBlockElement::GetByID($item_id);
		if (!$product_item = $dbr_item->GetNext()) {
			throw new Exception(Loc::getMessage('OC_FORM_PRODUCT_NOT_FOUND'));
		}

		$this->item = $product_item;
		$quantity = $this->form_data['qty'];
		if ($this->arParams['USE_RATIO']) {
			$ratio = $this->getProductRatio($item_id);
			$quantity = $quantity / $ratio;
		}
		$this->item['QUANTITY_RATIO'] = $quantity;

		$prices = $this->getOptimalPrices($item_id, $quantity);

		$this->item['PRICE'] = $prices['price'];
		if (CModule::IncludeModule('currency')) {
			$this->item['PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($prices['price'], $prices['currency']);
		} else {
			$this->item['PRICE_FORMATED'] = $prices['price'];
		}

		$basket = Basket::create($site_id);
		$item = $basket->createItem("catalog", $this->form_data['item_id']);
		$item->setFields([
			'PRODUCT_ID' => $product_item['ID'],
			'NAME' => $product_item['NAME'],
			'PRICE' => $prices['price'],
			'CURRENCY' => $prices['currency'],
			'QUANTITY' => $quantity,
			'LID' => $site_id,
			'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
		]);

		$person_type = intval($this->arParams['ORDER_PERSON_TYPE']);
		$default_user = intval($this->arParams['ORDER_DEFAULT_USER']);
		$payment_id = intval($this->arParams['ORDER_PAYMENT_ID']);
		$delivery_id = intval($this->arParams['ORDER_DELIVERY_ID']);

		$order_user = $USER->isAuthorized() ? $USER->GetID() : ($default_user ? $default_user : $this->default_user);

		$order = Order::create($site_id, $order_user);
		$order->setPersonTypeId($person_type ? $person_type : $this->person_type);
		$order->setField('USER_DESCRIPTION', Loc::getMessage('OC_FORM_USER_DESCRIPTION'));
		$order->setBasket($basket);

		$shipment_collection = $order->getShipmentCollection();
		if ($delivery_id) {
			$shipment = $shipment_collection->createItem(
				Delivery\Services\Manager::getObjectById($delivery_id)
			);
		} else {
			$shipment = $shipment_collection->createItem();
			$service = Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
			$shipment->setFields([
				'DELIVERY_ID' => $service['ID'],
				'DELIVERY_NAME' => $service['NAME'],
			]);
		}
		foreach ($basket as $basket_item) {
			$shipment_item_collection = $shipment->getShipmentItemCollection();
			$shipment_item = $shipment_item_collection->createItem($basket_item);
			$shipment_item->setQuantity($item->getQuantity());
		}

		$payment_collection = $order->getPaymentCollection();
		$payment = $payment_collection->createItem();
		$pay_system_service = PaySystem\Manager::getObjectById($payment_id ? $payment_id : $this->payment_id);
		$payment->setFields([
			'PAY_SYSTEM_ID' => $pay_system_service->getField("ID"),
			'PAY_SYSTEM_NAME' => $pay_system_service->getField("NAME")
		]);

		$property_collection = $order->getPropertyCollection();
		foreach (['NAME', 'EMAIL', 'PHONE'] as $code) {
			if ($param_code = $this->arParams["ORDER_CODE_{$code}"]) {
				$prop = $this->getPropertyByCode($property_collection, $param_code);
				if ($prop !== null) {
					$prop->setValue($this->form_data[strtolower($code)]);
				}
			}
		}

		$order->doFinalAction(true);
		$result = $order->save();
		if (!$result->isSuccess()) {
			throw new Exception(implode(', ', $result->getErrors()));
		}
		return $order->getId();
	}

	/**
	 * @param $property_collection
	 * @param $code
	 * @return null
	 */
	private function getPropertyByCode($property_collection, $code)  {
		foreach ($property_collection as $property) {
			if($property->getField('CODE') == $code) return $property;
		}
		return null;
	}

	/**
	 * @return array
	 */
	private function validateFormData() {
		global $APPLICATION, $USER;

		$errors = [];
		$fields = [
			'item_id' => true,
			'qty' => true,
			'name' => true,
			'email' => true,
			'phone' => false,
			'captcha' => true,
			'sess_id' => false,
			'captcha_sid' => false
		];
		$form_data = $this->getRequestData(array_keys($fields));

		if (!$form_data['sess_id'] || bitrix_sessid() != $form_data['sess_id']) {
			$errors[] = Loc::getMessage('OC_FORM_VALIDATE_ERROR_SESSION');
		}

		if ($this->arParams['USE_CAPTCHA'] == 'Y' && !$USER->IsAuthorized()) {
			if ($form_data['captcha'] && (!$form_data['captcha_sid'] || !$APPLICATION->CaptchaCheckCode($form_data['captcha'], $form_data['captcha_sid']))) {
				$errors[] = Loc::getMessage('OC_FORM_VALIDATE_ERROR_CAPTCHA');
			}
		} else {
			$fields['captcha'] = false;
		}

		$empty_required = [];
		foreach ($fields as $key => $required) {
			if ($required && !$form_data[$key]) {
				$empty_required[] = Loc::getMessage('OC_FORM_VALIDATE_FIELD_'.strtoupper($key));
			}
		}
		if (!empty($empty_required)) {
			$errors[] = Loc::getMessage('OC_FORM_VALIDATE_ERROR_EMPTY', ['#FIELDS#' => '"'.implode('", "', $empty_required).'"']);
		}

		if ($form_data['email'] && !check_email($form_data['email'])) {
			$errors[] = Loc::getMessage('OC_FORM_VALIDATE_ERROR_EMAIL');
		}
		return $errors;
	}

	/**
	 * @param bool $keys
	 * @return array
	 */
	private function getRequestData($keys = false) {
		if (!$keys) $keys = array_keys($_REQUEST);
		$result = [];
		foreach ($keys as $key) {
			$result[$key] = isset($_REQUEST[$key]) ? trim(htmlspecialcharsex($_REQUEST[$key])) : '';
		}
		if (SITE_CHARSET == 'windows-1251') {
			$result = utf8win1251($result);
		}
		$this->form_data = $result;
		return $result;
	}

	/**
	 * @param bool $data
	 */
	private function response($data = false) {
		global $APPLICATION;
		$arResult = $data ? $data : $this->arResult;
		if (SITE_CHARSET == 'windows-1251') {
			array_walk_recursive($arResult, function (&$value) {
				$value = iconv('windows-1251', 'utf-8', $value);
			});
		}
		$APPLICATION->RestartBuffer();
		header('Content-Type: application/json');
		$this->output = json_encode($arResult);
		AddEventHandler('main', 'OnEndBufferContent', [$this, 'getOutput'], 50000);
		exit();
	}

	/**
	 * @param $content
	 */
	public function getOutput(&$content) {
		$content = $this->output;
	}

	/**
	 *  captcha.php
	 */
	public function fileCaptcha() {
		global $APPLICATION;
		$this->response(['captcha' => $APPLICATION->CaptchaGetCode()]);
	}

	/**
	 *  price.php
	 */
	public function filePrice() {

		if (CModule::IncludeModule("catalog")) {

			$product_id = intval($_REQUEST['product_id']);
			$quantity = intval($_REQUEST['product_qty']);
			$template = htmlspecialcharsex($_REQUEST['template']);
			if (SITE_CHARSET == 'windows-1251') {
				$template = utf8win1251($template);
			}
			$currency = htmlspecialcharsex($_REQUEST['currency']);

			if ($_REQUEST['use_ratio'] == 'Y') {
				$ratio = $this->getProductRatio($product_id);
				$quantity = $quantity / $ratio;
			}

			$prices = $this->getOptimalPrices($product_id, $quantity);
			$discount_price = $prices['price'];
			$old_price = $prices['old_price'];

			if (CModule::IncludeModule('currency')) {
				$text = str_ireplace(
					[
						'#DISCOUNT_PRICE#',
						'#OLD_PRICE#',
						'#CURRENCY#'
					], [
					CCurrencyLang::CurrencyFormat($discount_price, $currency),
					$old_price ? CCurrencyLang::CurrencyFormat($old_price, $currency) : '',
					''
				],
					$template
				);
			} else {
				$text = str_ireplace(
					[
						'#DISCOUNT_PRICE#',
						'#OLD_PRICE#',
						'#CURRENCY#'
					], [
					$discount_price,
					$old_price,
					'<i class="fa fa-'.strtolower($currency).'"></i>'
				],
					$template
				);

			}
		} else {
			$text = '';
		}
		$this->response(['text' => $text]);
	}

	/**
	 * @param $product_id
	 * @return int
	 * @throws \Bitrix\Main\ArgumentException
	 */
	private function getProductRatio($product_id) {
		$ratio = 1;
		$measure = ProductTable::getCurrentRatioWithMeasure([$product_id]);
		if (!empty($measure) && isset($measure[$product_id]['RATIO'])) {
			$ratio = $measure[$product_id]['RATIO'];
		}
		return $ratio;
	}

	/**
	 * @param $product_id
	 * @param $quantity
	 * @return array
	 */
	private function getOptimalPrices($product_id, $quantity) {
		$optimal_price = CCatalogProduct::GetOptimalPrice($product_id, $quantity);
		$old_price = 0;
		if (isset($optimal_price['RESULT_PRICE']) && is_array($optimal_price['RESULT_PRICE'])) {
			$price = $optimal_price['RESULT_PRICE'];
			$currency = $price['CURRENCY'];
			if ($price['DISCOUNT_PRICE'] && $price['BASE_PRICE'] != $price['DISCOUNT_PRICE']) {
				$discount_price = $price['DISCOUNT_PRICE'];
				$old_price = $price['BASE_PRICE'];
			} else {
				$discount_price = $price['BASE_PRICE'];
			}
		} else {
			$currency = $optimal_price['PRICE']['CURRENCY'];
			if ($optimal_price['DISCOUNT_PRICE'] && $optimal_price['PRICE']['PRICE'] != $optimal_price['DISCOUNT_PRICE']) {
				$discount_price = $optimal_price['DISCOUNT_PRICE'];
				$old_price = $optimal_price['PRICE']['PRICE'];
			} else {
				$discount_price = $optimal_price['PRICE']['PRICE'];
			}
		}

		return [
			'price' => $discount_price * $quantity,
			'old_price' => $old_price * $quantity,
			'currency' => $currency
		];
	}

};


